define ->
  return {
  cities:
    items: [
      {
        code: 'spb'
        name: 'Санкт-Петербург'
      },
      {
        code: 'msk'
        name: 'Москва'
      },
      {
        code: 'uralsk'
        name: 'Уральск'
      }
    ]
    initial: 'uralsk'
  }
  