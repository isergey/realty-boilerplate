define [
  'marionette',
  'underscore',
  'jquery',
  './field'
], (Marionette, _, $, Field) ->

  class Form extends Marionette.ItemView
    initialize: (args) ->
      @title = args.title || ''
      console.log('title', @title)
      @mixValue = {}

      @fieldViews = args.fieldViews || []

      for field in @fieldViews
        @_listenFieldEvents(field)

      @_listenEvents()


    render: ->
      if @title.length > 0
        @$el.append("<span class='form__title'>#{@title}</span>")
      $fieldsEl = $('<ul></ul>')
      for field in @fieldViews
        $fieldsEl.append(field.render().el)
      @$el.append($fieldsEl)
      return @


    addField: (fieldView, andRender = false) ->
      @fieldViews.push(fieldView)
      @_listenFieldEvents(fieldView)
      if andRender
        @$el.append(fieldView.render().el)


    getValue: ->
      value = {}
      for field in @fieldViews
        if field.isActive()
          fieldValue = field.getValue()
          if fieldValue == undefined
            continue
          if _.isArray(fieldValue) and fieldValue.length == 0
            continue
          value[field.getName()] = fieldValue

      for key, val of @mixValue
        value[key] = val

      return value

    mixToValue: (value) ->
      @mixValue = value
      @trigger('change')

    _listenEvents: ->
      self = @
      @on('close', ->
        for field in self.fieldViews
          field.close()
      )

      @on('change', ->
        value = @getValue()
        for field in @fieldViews
          field.changeActive(value)
      )

    _listenFieldEvents: (fieldView) ->
      self = @
      fieldView.on('value:change', (value) ->
        self.trigger('change', value)
      )

  return {
  Form: Form
  }

