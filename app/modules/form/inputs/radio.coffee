define [
  'marionette',
  'underscore',
  'jquery'
], (Marionette, _, $) ->
  class RadioInput extends Marionette.ItemView

    events: {
      'change input': 'changeValue'
    }


    initialize: (args) ->
      @choices = args.choices || []

      @initial = args.initial
      @initialHash = {}

      if @initial != undefined
        if _.isArray(@initial)
          for value in @initial
            @initialHash[value] = true
        else
          @initialHash[@initial] = true



    render: ->
      index = 0
      for choice in @choices
        $radioEl = $(
          "<div><label for='#{@cid}-#{index}'><input id='#{@cid}-#{index}' type='radio' name='#{@cid}' value='#{choice.value}'>#{choice.title}</label></div>")
        console.log('@initialHash', @initialHash[choice.value], choice.value)
        if @initialHash[choice.value] != undefined
          $radioEl.find('input').attr('checked', 'checked')
        @$el.append($radioEl)
        index += 1
      return @


    getValue: ->
      return @$el.find('input:checked').val()


    changeValue: ->
      @trigger('change', @getValue())

  return RadioInput