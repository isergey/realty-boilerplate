define [
  'marionette',
  'underscore',
  'jquery'
], (Marionette, _, $) ->
  class RangeInput extends Marionette.ItemView

    events: {
      'change input': 'changeValue'
    }


    initialize: (args) ->
      @initial = args.initial || ['', '']

      if not _.isArray(@initial)
        throw new Error('initial must by array')

      if @initial.length < 2
        throw new Error('"initial" array must contain 2 elements (from, to)')

    render: ->
      @$el.html("от <input class='range-from' value='#{@initial[0]}' /><br/> до <input class='range-to' value='#{@initial[1]}' />")
      return @


    getValue: ->
      return [@$el.find('.range-from').val(), @$el.find('.range-to').val()]


    changeValue: ->
      @trigger('change', @getValue())

  return RangeInput