define [
  'marionette',
  'underscore',
  'jquery'
], (Marionette, _, $) ->
  class TextInput extends Marionette.ItemView

    events: {
      'change input': 'changeValue'
    }

    initialize: (args) ->
      @initial = args.initial
      if @initial == undefined
        @initial = ''

    render: ->
      @$el.html("<input type='text' value='#{@initial}'/>")
      return @

    getValue: ->
      return @$el.find('input').val()

    changeValue: ->
      @trigger('change', @getValue())

  return TextInput