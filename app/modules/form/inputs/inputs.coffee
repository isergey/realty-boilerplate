define [
  'marionette',
  'underscore',
  'jquery',
  './checkbox',
  './radio',
  './range',
  './text',
], (Marionette, _, $, CheckboxInput, RadioInput, RangeInput, TextInput) ->
  return {
  CheckboxInput: CheckboxInput
  RadioInput: RadioInput
  RangeInput: RangeInput
  TextInput: TextInput
  }