define [
  'marionette',
  'underscore',
  'jquery'
], (Marionette, _, $) ->
  class CheckboxInput extends Marionette.ItemView

    events: {
      'change input': 'changeValue'
    }


    initialize: (args) ->
      @choices = args.choices || []
      @initial = args.initial
      @initialHash = {}

      if @initial != undefined
        if _.isArray(@initial)
          for value in @initial
            @initialHash[value] = true
        else
          @initialHash[@initial] = true


    render: ->
      index = 0
      for choice in @choices
        $choiceEl = $("<div><label for='#{@cid}-#{index}'><input id='#{@cid}-#{index}' type='checkbox' name='#{@cid}' value='#{choice.value}'>#{choice.title}</label></div>")
        if @initialHash[choice.value] != undefined
          $choiceEl.attr('checked', 'checked')
        @$el.append($choiceEl)
        index += 1
      return @


    getValue: ->
      values = []
      @$el.find('input:checked').each(->
        values.push($(@).val())
      )
      return values


    changeValue: ->
      @trigger('change', @getValue())

  return CheckboxInput