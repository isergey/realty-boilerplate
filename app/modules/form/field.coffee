define [
  'marionette',
  'underscore',
  'jquery',
  './inputs/inputs'
], (Marionette, _, $, inputs) ->
  class Field extends Marionette.ItemView
    tagName: 'li'
    className: 'form-field'

    events:
      'click .form-field__field-title, .form-field__collapse-toggler': 'toggleCollapse'

    initialize: (args) ->
      @name = args.name
      if @name == undefined
        throw new Exception("Field must have name arg")


      @title = args.title
      @collapsable = if args.collapsable == undefined then false else args.collapsable
      @collapsed = if args.collapsed == undefined then true else args.collapsed

      @initial = args.initial
      @inputView = null

      if args.input == undefined
        console.warn("Not input not defined in args")
        @inputView = new inputs.TextInput(args)
      else
        if not inputs[args.input]
          throw new Error("Not registerd input", args.input)
        @inputView = new inputs[args.input](args)

      @deactivateValues = args.deactivateValues || {}
      @active = true

      @_listenEvents()


    render: ->
      @$el.html('')
      if @collapsable
        $collapseToglerEl = $('<span class="form-field__collapse-toggler"></span>')
        if @collapsed
          console.log('@collapsed1', @collapsed)
          $collapseToglerEl.addClass('form-field__collapse-toggler_collapsed')
        @$el.append($collapseToglerEl)

      $titleEl = $("<span class='form-field__field-title'>#{@title}</span>")
      if @collapsable
        $titleEl.addClass('form-field__field-title_collapsable')

      @$el.append($titleEl)
      @$el.append(@inputView.render().el)

      if @collapsable and @collapsed
        @inputView.$el.hide()

      return @

    toggleCollapse: ->
      @$el.find('.form-field__collapse-toggler').toggleClass('form-field__collapse-toggler_collapsed')
      if @collapsable
        @inputView.$el.toggle(100)

    onClose: ->
      @inputView.close()


    getName: ->
      return @name


    getValue: ->
      return @inputView.getValue()


    toDict: ->
      return {
      name: @name
      value: @inputView.getValue()
      }


    isActive: ->
      return @active


    setActivate: (active) ->
      if not @active and active
        @active = true
        @$el.removeClass('form-field_deactive')

      if @active and not active
        @active = false
        @$el.addClass('form-field_deactive')
      return @


    changeActive: (formValue) ->
      beActive = true

      for name, deactivateValue of @deactivateValues
        deactivateValueHash = {}
        if _.isArray(deactivateValue)
          for deactivateValueItem in deactivateValue
            deactivateValueHash[deactivateValueItem] = null
        else
          deactivateValueHash[deactivateValue] = null

        if name == @name
          continue

        formFieldValue = formValue[name]
        if formFieldValue == undefined
          continue
        if _.isArray(formFieldValue)
          for formFieldValueItem in formFieldValue
            if deactivateValueHash[formFieldValueItem] != undefined
              beActive = false
              break
        else
          if deactivateValueHash[formFieldValue] != undefined
            beActive = false
            break

      if beActive
        @setActivate(true)
      else
        @setActivate(false)

      return @

    _listenEvents: ->
      self = @

      @inputView.on('change', (value) ->
        self.trigger('value:change', value)
      )


  return Field