define [
  'marionette',
  'underscore',
  'jquery'
], (Marionette, _, $) ->
  class Map extends Marionette.ItemView
    className: 'map'

    initialize: (args) ->
      console.log('args', args.center)
      @center = args.center || [0, 0]
      @map = @_initMap()

    _initMap: ->
      return new ymaps.Map(@el, {
        center: @center,
        zoom: 7,
        controls: ['trafficControl', 'typeSelector', 'zoomControl', 'routeEditor']
      })

    render: ->
      if @map == null
        @map = @_initMap()
      return @

    onClose: ->
      @map = null

    addPoints: (points) ->
      for point in points
        geoPoint = new ymaps.Placemark(point.coordinates, {
          hintContent: 'Москва!',
          balloonContent: 'Столица России'
        })
        @map.geoObjects.add(geoPoint)

  return Map
