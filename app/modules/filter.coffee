define [
  'marionette',
  'underscore',
  'jquery',
  '../app_state',
  './form/form',
  './form/field',
  'hbs!templates/filter/layout'
], (Marionette, _, $, appState, form, Field, LayoutTpl) ->
  class FilterLayout extends Marionette.Layout
    className: 'filter'
    template: LayoutTpl

    regions:
      contentPanel: '.filter__form'

    events:
      'click button': 'submit'
      'click .filter__toggle-panel': 'toggle'

    toggle: ->
      @$el.parent().toggleClass('show');

    toggleHide: ->
      @$el.parent().removeClass('show')

    toggleShow: ->
      @$el.parent().addClass('show')

    onShow: ->
      self = @
      fieldViews = [
        new Field({
          name: 'object_type',
          input: 'RadioInput',
          title: 'Тип объекта',
          choices: [
            {
              title: 'Квартира',
              value: 'flat'
            },
            {
              title: 'Комната',
              value: 'room'
            },
            {
              title: 'Офис',
              value: 'office'
            }
          ]
          initial: 'flat'
        }),
        new Field({
          name: 'period',
          input: 'RadioInput',
          title: 'Период',
          choices: [
            {
              title: 'Длительно',
              value: 'long'
            },
            {
              title: 'Посуточно',
              value: 'short'
            }
          ]
          initial: 'long'
        }),
        new Field({
          name: 'price',
          input: 'RangeInput',
          title: 'Цена',
          initial: ['5000', '20000']
        }),
        new Field({
          name: 'rooms',
          input: 'CheckboxInput',
          title: 'Количество комнат',
          collapsable: true,
          choices: [
            {
              title: '1',
              value: '1'
            },
            {
              title: '2',
              value: '2'
            },
            {
              title: '3',
              value: '3'
            }
          ]
          deactivateValues:
            'object_type': ['room', 'office']
        }),
        new Field({
          name: 'metro',
          input: 'CheckboxInput',
          title: 'Метро',
          collapsable: true,
#          collapsed: true,
          choices: [
            {
              title: 'Академическая',
              value: '1'
            },
            {
              title: 'Лестная',
              value: '2'
            },
            {
              title: 'Невский проспект',
              value: '3'
            },
          ]
          deactivateValues:
            'city': ['uralsk']
        })
      ]
      formView = new form.Form({
        title: '',
        fieldViews: fieldViews
      })

      appState.on('change:city', ->
        formView.mixToValue({
          city: appState.get('city')
        })
      )

      @on('submit', ->
        self.trigger('submit:filter', formView.getValue())
        console.log('get form value', formView.getValue())
      )
      @contentPanel.show(formView)

    submit: (event) ->
      event.stopPropagation()
      @trigger('submit')

  return FilterLayout
