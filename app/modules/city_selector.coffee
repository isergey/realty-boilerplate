define [
  'backbone',
  'marionette',
  'underscore',
  'jquery',
  'modules/ui/modal/window'
], (Backbone, Marionette, _, $, ModalWindow) ->
  class CityModel extends Backbone.Model
    defaults:
      code: ''
      name: ''


  class CitiesCollection extends Backbone.Collection
    model: CityModel


  class CitiesListItemView extends Marionette.ItemView
    tagName: 'li'
    template: _.template('<%- name %>')
    events:
      'click': 'click'


    click: ->
      @trigger('click', @model)


  class CitiesListView extends Marionette.CollectionView
    tagName: 'ul'
    className: 'city-selector__cities-list'
    itemView: CitiesListItemView
    onAfterItemAdded: (itemView) ->
      self = @
      itemView.on('click', (itemModel) ->
        self.trigger('item:click', itemModel)
      )


  class CitySelector extends Marionette.ItemView
    className: 'city-selector'
    template: _.template('<span><%- name %></span>')
    events:
      'click': 'click'

    modelEvents:
      change: "modelChanged"


    initialize: (args) ->
      @cities = args.cities || []
      @initial = args.initial

      @model = new CityModel({})

      for city in @cities
        console.log(city.code, @initial)
        if city.code == @initial
          @model = new CityModel(city)
          break


    getValue: ->
      return @model.toJSON()


    click: ->
      self = @
      md = new ModalWindow({})
      md.show()

      md.header.show(new Marionette.ItemView({
        tagName: 'span'
        template: _.template('Список городов')
      }))
      clv = new CitiesListView({
        collection: new CitiesCollection(@cities)
      })
      clv.on('item:click', (itemModel) ->
        self.model.set(itemModel.toJSON())
        md.close()

      )
      md.body.show(clv)

    modelChanged: ->
      @render()
      @trigger('value:change', @model.toJSON())

  return CitySelector