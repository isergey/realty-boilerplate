define [
  'marionette',
  'underscore',
  'jquery',
  'hbs!templates/ui/modal/window'
], (Marionette, _, $, windowTpl) ->
  class Window extends Marionette.Layout
    className: 'md-modal'
    template: windowTpl

    regions:
      header: '.md-header'
      body: '.md-body'
      footer: '.md-footer'


    show: (cssEffect = "default")->
      self = @
      @render()
      $body = $('body')

      $body.append(@el)
      @$el.addClass("md-effect-#{cssEffect}")
      @$el.addClass('md-show')

      $overlay = $('<div class="md-overlay"></div>')
      $body.append($overlay)

      @on('close', ->
        $overlay.remove()
      )

      $overlay.on('click', ->
        self.$el.removeClass('md-show')
        setTimeout(->
          self.close()
        , 500)
      )
      return @


  return Window