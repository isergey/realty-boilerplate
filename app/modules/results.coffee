define [
  'backbone',
  'marionette',
  'underscore',
  'jquery',
  'hbs!templates/results/results_row'
], (Backbone, Marionette, _, $, ResultsRowTpl) ->
  class ResultsRowModel extends Backbone.Model
    defaults:
      price: 0,
      currency: 'руб.'
      title: ''
      comments: ''

  class ResultsCollection extends Backbone.Collection
    model: ResultsRowModel


  class ResultsRowView extends Marionette.ItemView
    tagName: 'tr'
    className: 'results-item'
    template: ResultsRowTpl
    events:
      'mouseenter': 'mouseenter'
      'mouseleave': 'mouseleave'

    mouseenter: ->
      @trigger('item:mouseenter')

    mouseleave: ->
      @trigger('item:mouseleave')


  class ResultsView extends Marionette.CollectionView
    className: 'results-list'
    tagName: 'table'
    itemView: ResultsRowView


  return {
  ResultsView: ResultsView
  ResultsCollection: ResultsCollection
  }


