define [
  'backbone',
  'params',
], (Backbone, params) ->
  class AppSate extends Backbone.Model
    defaults:
      city: 'spb'

  appSate = new AppSate({})
  appSate.on('change', ->
    console.log('app state change')
  )
  return appSate
