define [
  'marionette',
  'tpl!templates/app_layout.jtpl'
], (Marionette, AppLyoutTpl) ->
  class AppLayout extends Marionette.Layout
    template: AppLyoutTpl


  return AppLayout


