define ['marionette'], (Marionette) ->
  class Router extends Marionette.AppRouter
    routes:
      '': "index",
      "users": "users"
    
    index: ->
      window.console.log("Welcome to your / route.");

    users: ->
      window.console.log("Users rout")

  return Router


