require.config
  paths:
    'vendor': '../vendor',
    'almond': '../vendor/bower/almond/almond',
    'underscore': '../vendor/bower/lodash/dist/lodash.underscore',
    'jquery': '../vendor/bower/jquery/dist/jquery',
    'backbone': '../vendor/bower/backbone/backbone',
    'marionette': '../vendor/bower/backbone.marionette/lib/core/amd/backbone.marionette',
    'backbone.wreqr' : '../vendor/bower/backbone.wreqr/lib/backbone.wreqr',
    'backbone.eventbinder' : '../vendor/bower/backbone.eventbinder/lib/backbone.eventbinder',
    'backbone.babysitter' : '../vendor/bower/backbone.babysitter/lib/backbone.babysitter',
    'hbs': '../vendor/bower/require-handlebars-plugin/hbs',
    'tpl': "../vendor/bower/requirejs-tpl/tpl"
