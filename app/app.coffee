define [
  'backbone',
  'marionette',
  'router',
  'params',
  'app_state',
  'modules/city_selector',
  'modules/map',
  'modules/results',
  'modules/filter',
  'modules/ui/modal/window'
  'app_layout'
], (Backbone, Marionette, Router, params, appState, CitySelector, Map, Results, FilterLayout, ModalWindow, AppLayout) ->
  app = new Marionette.Application()

  app.addRegions(
    citySelector: '.city-selector',
    mapRegion: '.map-region',
    filterRegion: '.filter-region',
    resultsRegion: '.results-region'
  )

  app.on("initialize:after", ->
    app.router = new Router();

    if Backbone.history
      Backbone.history.start({ pushState: true, root: '/' })

    citySelector = new CitySelector(
      cities: params.cities.items
      initial: params.cities.initial
    )

    citySelector.on('value:change', (value) ->
      appState.set('city', value.code)
    )

    app.citySelector.show(citySelector)
    filter = new FilterLayout({})
    filter.on('submit:filter', (value) ->
      filter.toggleHide()
    )
    app.filterRegion.show(filter)

    ymaps.ready(->
      map = new Map({
        center: [59.941422, 30.307661]
      })


      app.mapRegion.show(map);
      map.addPoints([
        {
          coordinates: [59.991682, 30.377699]
        },
        {
          coordinates: [59.917297, 30.307661]
        },
      ])
    )

    resultsCollection = new Results.ResultsCollection([])
    resultsView = new Results.ResultsView(
      collection: resultsCollection
    )
    app.resultsRegion.show(resultsView)
    id = 1
    id++
    resultsCollection.add(
      id: id++,
      title: '2 комн.: 80.00/–/12.00м², этаж 5/10',
      price: 28000,
      comments: 'Россия, Санкт-Петербург, Коллонтай ул. м. Проспект Большевиков, 10 мин. пешком'
    )
  )

  return app

